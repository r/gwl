(import (ice-9 match))

(define %prefix
  (string-append "/tmp/gwl-slurm"))

(define %munge-prefix
  (string-append %prefix "/munge"))

(define (mkdir-p dir)
  "Create directory DIR and all its ancestors."
  (define absolute?
    (string-prefix? "/" dir))

  (define not-slash
    (char-set-complement (char-set #\/)))

  (let loop ((components (string-tokenize dir not-slash))
             (root       (if absolute?
                             ""
                             ".")))
    (match components
      ((head tail ...)
       (let ((path (string-append root "/" head)))
         (catch 'system-error
           (lambda ()
             (mkdir path)
             (loop tail path))
           (lambda args
             (if (= EEXIST (system-error-errno args))
                 (loop tail path)
                 (apply throw args))))))
      (() #t))))


(define (init-munge!)
  (mkdir-p %munge-prefix)
  (system* "mungekey" "-k" (string-append %munge-prefix "/munge.key")))

(define munged-command
  (list "munged"
        "-F"
        (string-append "--pid-file=" %munge-prefix "/munged.pid")
        (string-append "--seed-file=" %munge-prefix "/munged.seed")
        (string-append "--key-file=" %munge-prefix "/munge.key")
        (string-append "--socket=" %munge-prefix "/munged.socket")
        "--num-threads=10"))

(define* (slurm.conf #:optional out)
  (define username (or (getlogin) "nobody"))
  (format out
          "\
ClusterName=gwl-test

AuthType=auth/munge
AuthInfo=~a/munged.socket
FirstJobId=65536
InactiveLimit=120
JobCompType=jobcomp/filetxt
JobCompLoc=~a/jobcomp
KillWait=30
MaxJobCount=10000
MinJobAge=3600
ReturnToService=0
SchedulerType=sched/backfill

SlurmdUser=~a
SlurmdDebug=debug
SlurmdLogFile=~a/slurmd.log
SlurmdPort=7003
SlurmdPidFile=~a/slurmd.pid
SlurmdSpoolDir=~a/slurmd.spool

SlurmUser=~a
SlurmctldHost=localhost(127.0.0.1)  # Primary server
SlurmctldPort=7002
SlurmctldDebug=debug
SlurmctldLogFile=~a/slurmctld.log
SlurmctldPidFile=~a/slurmctld.pid

StateSaveLocation=~a/slurm.state
SwitchType=switch/none
TmpFS=/tmp
WaitTime=30
ProctrackType=proctrack/linuxproc
JobAcctGatherType=jobacct_gather/none

#
# Node Configurations
#
NodeName=DEFAULT CPUs=4 Sockets=1 CoresPerSocket=2 ThreadsPerCore=2 RealMemory=2000 TmpDisk=64000 State=UNKNOWN
NodeName=localhost NodeAddr=127.0.0.1 Weight=16

# Partition Configurations
#
PartitionName=DEFAULT MaxTime=30 MaxNodes=10 State=UP
PartitionName=debug Nodes=ALL Default=YES
"
          %munge-prefix
          %prefix
          username
          %prefix
          %prefix
          %prefix
          username
          %prefix
          %prefix
          %prefix))

(define (init-slurm!)
  (mkdir-p %prefix)
  (mkdir-p (string-append %prefix "/slurmd.spool"))
  (call-with-output-file (string-append %prefix "/slurm.conf")
    slurm.conf))

(define slurmd-command
  (list "slurmd" "-D" "-f" (string-append %prefix "/slurm.conf")))

(define slurmctld-command
  (list "slurmctld" "-D" "-f" (string-append %prefix "/slurm.conf")))

(define (spawn command)
  (match (primitive-fork)
    (0
     (dynamic-wind
       (const #t)
       (lambda ()
         (apply execlp (car command) command))
       (lambda ()
         (primitive-exit 127))))
    (pid pid)))

(define (start . _)
  (init-slurm!)
  (init-munge!)

  ;; Must be set when using DRMAA
  (setenv "SLURM_CONF" (string-append %prefix "/slurm.conf"))
  (setenv "GUILE_DRMAA_LIBRARY"
          (string-append (getenv "GUIX_ENVIRONMENT")
                         "/lib/libdrmaa.so.1.0.8"))

  (spawn munged-command)
  (spawn slurmctld-command)
  (spawn slurmd-command)
  (waitpid WAIT_ANY))

(define (stop . _)
  (define* (read-pid-file file #:optional (prefix %prefix))
    (call-with-input-file (string-append prefix "/" file) read))
  (for-each (lambda (pid)
              (false-if-exception (kill pid SIGTERM)))
            (list
             (read-pid-file "slurmctld.pid")
             (read-pid-file "slurmd.pid")
             (read-pid-file "munged.pid" %munge-prefix))))
